import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.component.html',
  styleUrls: ['./publicaciones.component.css']
})
export class PublicacionesComponent implements OnInit {

  constructor(private router: Router) { }

  link = "https://concepto.de/wp-content/uploads/2013/08/matematicas-e1551990337130.jpg";
  p = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus dignissimos explicabo inventore, perferendis quisquam at dolor odit doloremque fugiat, exercitationem, ex minus aliquid! Earum ab cumque voluptate quos suscipit id! Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus dignissimos explicabo inventore, perferendis quisquam at dolor odit doloremque fugiat, exercitationem, ex minus aliquid! Earum ab cumque voluptate quos suscipit id!";

  publicaciones: any = [
    {isFavorite: true, number: 15, title: '¿Cómo programar?', stitle: 'Juan Perez', image: this.link, text: this.p},
    {isFavorite: false, number: 25, title: '¿Cómo resolver diferenciales?', stitle: 'Juan Lopez', image: this.link, text: this.p},
    {isFavorite: false, number: 40, title: 'TOEFL Book', stitle: 'Pedro Perez', image: this.link, text: this.p},
    {isFavorite: true, number: 80, title: 'Necesito ayuda con esta tarea', stitle: 'Jose', image: this.link, text: this.p},
    {isFavorite: false, number: 7, title: 'Programación con Java', stitle: 'Uranai', image: this.link, text: this.p},
    {isFavorite: true, number: 2, title: 'Clases y funciones', stitle: 'Garlick', image: this.link, text: this.p},
    {isFavorite: false, number: 1, title: 'Resumen historia', stitle: 'Teemo', image: this.link, text: this.p},
    {isFavorite: true, number: 27, title: '¿Cómo usar Office?', stitle: 'kled', image: this.link, text: this.p}
  ]
  // navigate(){
  //   this.router.navigateByUrl(`publicacion1`)
  // }

  cambioCor(i){
    if(!this.publicaciones[i].isFavorite){
      this.publicaciones[i].number = this.publicaciones[i].number + 1;
      this.publicaciones[i].isFavorite = !this.publicaciones[i].isFavorite;
    }else{
      this.publicaciones[i].number = this.publicaciones[i].number - 1;
      this.publicaciones[i].isFavorite = !this.publicaciones[i].isFavorite;
    }
  }

  ngOnInit(): void {
  }

 

}
